Feature: Demo Karate

Background:
  # this section is optional !
  # steps here are executed before each Scenario in this file
  # variables defined here will be 'global' to all scenarios
  # and will be re-initialized before every scenario
  Given def myVar = 'This is a example of using variables in the Background environment'
  Given def globalVar = karate.properties['config']
  Then print myVar
  Then print globalVar.appId

Scenario: Testing valid GET endpoint
Given url 'http://cul.esla.internal.services.rccl.com/esl/search/rest/cruiseSearch?header.application=celebritycruises.com&header.brand=C&header.domainId=6&header.language=en_US&criteria.office=MIA&criteria.country=USA&criteria.currency=USD&criteria.channel=VP&criteria.bookingType=FIT&criteria.cruiseType.value=CO,CT&criteria.brand.value=C&qualifiers.priceScope=REQUESTED_AVAILABLE&qualifiers.searchScope=ALL&resultsPreference.groupBy=PACKAGE&resultsPreference.includeFacets=false&resultsPreference.includePrices=true&resultsPreference.includeResults=true&resultsPreference.pagination.count=5&resultsPreference.pagination.offset=0&resultsPreference.priceLevel.bestPricedCategory=true&resultsPreference.sortBy=PROMOTIONAL_DEALS&resultsPreference.sortOrder=ASCENDING&resultsPreference.strictSearch=true'
When method GET
Then status 200
And match /CruiseSearchResponse/header/status == 'Success'


Scenario: Testing status page
Given url 'http://cas-events-testing.itopia.com/api/Status'
When method GET
Then status 200