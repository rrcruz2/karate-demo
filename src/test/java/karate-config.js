function fn() {   
  var env = karate.env; // get java system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'dev'; // a custom 'intelligent' default
  }
  var config = { // base config JSON
    appId: 'com.karate.demo',
    appSecret: 'my.secret',
    cruiseSearchUrl: 'https://some-host.com/v1/auth/',
    eventPlatformUrl: 'http://cas-events-testing.itopia.com/api/Status'
    
  };
  if (env == 'stage') {
    // over-ride only those that need to be
    config.cruiseSearchUrl = 'https://some-host.com/v1/auth/';
    config.eventPlatformUrl = 'http://cas-events-testing.itopia.com/api/Status';
    
  } else if (env == 'e2e') {
	  config.cruiseSearchUrl = 'https://some-host.com/v1/auth/';
	  config.eventPlatformUrl = 'http://cas-events-testing.itopia.com/api/Status';
  }
  // don't waste time waiting for a connection or if servers don't respond within 5 seconds
  karate.configure('connectTimeout', 5000);
  karate.configure('readTimeout', 5000);
  return config;
}